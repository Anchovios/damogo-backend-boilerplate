use crate::context::Ctx;
use juniper::graphql_object;

pub struct Root;

#[graphql_object(Context = Ctx)]
impl Root {
    fn dummy() -> bool {
        true
    }
}
